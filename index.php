<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>カレンダー</title>
        <style>
            
        </style>
    </head>
    <body>
<?php
function calendar($year = '', $month = '') {
    if (empty($year) && empty($month)) {
        $year = date('Y');
        $month = date('n');
    }
    //月末の取得
    $l_day = date('j', mktime(0, 0, 0, $month + 1, 0, $year));
    //初期出力

    $html = <<<EOM
EOM;
    $lc = 0;
 
    // 月末分繰り返す
    for ($i = 1; $i < $l_day + 1;$i++) {
        $classes = array();
        $class   = '';
 
        // 曜日の取得
        $week = date('w', mktime(0, 0, 0, $month, $i, $year));
 
        // 曜日が日曜日の場合
        if ($week == 0) {
            $html .= "\t\t<tr>\n";
            $lc++;
        }
 
        // 1日の場合
        if ($i == 1) {
            if($week != 0) {
                $html .= "\t\t<tr>\n";
                $lc++;
            }
            $html .= repeatEmptyTd($week);
        }
 
        if ($week == 6) {
            $classes[] = 'sat';
        } else if ($week == 0) {
            $classes[] = 'sun';
        }
 
        if ($i == date('j') && $year == date('Y') && $month == date('n')) {
            // 現在の日付の場合
            $classes[] = 'today';
        }
 
        if (count($classes) > 0) {
            $class = ' class="'.implode(' ', $classes).'"';
        }
 
        $html .= "\t\t\t".'<td'.$class.'>'.$i.'</td>'."\n";
 
        // 月末の場合
        if ($i == $l_day) {
            $html .= repeatEmptyTd(6 - $week);
        }
        // 土曜日の場合
        if ($week == 6) {
            $html .= "\t\t</tr>\n";
        }
    }
 
    if ($lc < 6) {
        $html .= "\t\t<tr>\n";
        $html .= repeatEmptyTd(7);
        $html .= "\t\t</tr>\n";
    }
 
    if ($lc == 4) {
        $html .= "\t\t<tr>\n";
        $html .= repeatEmptyTd(7);
        $html .= "\t\t</tr>\n";
    }
 
    $html .= "\t</tbody>\n";
    $html .= "</table>\n";
 
    
    echo $html;
}
 
function repeatEmptyTd($n = 0) {
    return str_repeat("\t\t<td> </td>\n", $n);
}



if(isset($_POST['year'])&&isset($_POST['month'])){
          $year = $_POST['year'];
          $month = $_POST['month'];
}


?>
<table class="calendar">
    <thead>
        <tr>
            <td colspan="2"> </td>
            <th colspan="3"><?=$year?>年<?=$month?>月</th>
            <td colspan="2"> </td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th class="sun">日</th>
            <th>月</th>
            <th>火</th>
            <th>水</th>
            <th>木</th>
            <th>金</th>
            <th class="sat">土</th>
        </tr>
    <form method="post" action="" enctype="multipart/form-data">
    年:
   <input type="text" name="year" value="<?=$year?>"> 
    <br>
   月:
    <select size="1" name="month">
        <option value="1" <?php ?>>1</option>
     <option value="2">2</option>
     <option value="3">3</option>
     <option value="4">4</option>
     <option value="5">5</option>
     <option value="6">6</option> 
     <option value="7">7</option>
     <option value="8">8</option>
     <option value="9">9</option> 
     <option value="10">10</option>
     <option value="11">11</option>
     <option value="12">12</option> 
     </select>    
    <input type="submit"  value="変更">
</form>
<?php

calendar($year,$month);
?>


